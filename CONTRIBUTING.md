# How to
## Issues
If you have a [Framagit](https://framagit.org) account, you can directly create a new issue at the [issue board](https://framagit.org/tractor/tractor/-/issues).

You can also create a new issue by sending an email to the [project's issues email address](mailto:gitlab-incoming+tractor-tractor-67089-4tkz1e0f90cy2gilp6fnnpcfw-issue@framagit.org). The subject will be used as the title of the new issue, and the message will be the description. [Quick actions](https://framagit.org/help/user/project/quick_actions) and styling with [Markdown](https://framagit.org/help/user/markdown) are supported.

## Merge Requests
If you have a [Framagit](https://framagit.org) account, you can fork and send merge requests.

The second favorite model is using [git send-email](https://git-scm.com/docs/git-send-email) to send patches as issues to the issue board via email.

At last, you can reupload this project to the git host of your choice and send the MR link to the issue board via email.


# Areas of interests
There are some important areas you can contribute to this project:

## Code
* Expanding the test suit.

## Community
* Packaging for more GNU/Linux distributions.
* Packaging for [Homebrew](https://brew.sh) and [Msys2](https://www.msys2.org).
