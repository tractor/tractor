# Tractor

Tractor is core app which lets user to setup a proxy with Onion Routing via TOR and optionally obfs4proxy in their user space. The goal is to ease the proccess of connecting to TOR and prevent messing up with system files.


## Install
Tractor is natively packaged for some distros:

[![Packaging status](https://repology.org/badge/vertical-allrepos/tractor.svg)](https://repology.org/project/tractor/versions)

If you are any other distribution or OS, Tractor team welcomes you to make build recepie of yours.

You can also use use global package mangers like [GNU GUIX](https://packages.guix.gnu.org/packages/tractor) or [Python Pypi](https://pypi.org/project/traxtor)


## Run
you can run Tractor by command line or use one of the graphical interfaces which are packaged separately.

For command line interface, read manual provided with the package or available [online](https://framagit.org/tractor/tractor/-/blob/main/data/tractor.1).


## Contribute
Contibuting is always appreciated. See [CONTRIBUTING](https://framagit.org/tractor/tractor/-/blob/main/CONTRIBUTING.md) for more information.
